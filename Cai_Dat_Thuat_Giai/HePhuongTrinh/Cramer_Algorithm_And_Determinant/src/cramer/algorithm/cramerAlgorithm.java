package cramer.algorithm;


import calculate.determinant.*;

public class cramerAlgorithm {
	
	public cramerAlgorithm() {
		
		
	}
	
	public static void main(String[] args) {
        int n = 4;
		int[][] matrix = new int[n][n];
        matrix[0] = new int[] { 2, -3, 5, -1, 2 };
        matrix[1] = new int[] { -1, -2, 3, 4, 0 };
        matrix[2] = new int[] { 3, 8, -5, 3, -2 };
        matrix[3] = new int[] { 0, -4, 2, -7, 9 };
//		matrix[0] = new int[] { 3, 5, -1, -2 };
//        matrix[1] = new int[] { 1, -4, 2, 13 };
//        matrix[2] = new int[] { 2, 4, 3, 1 };
		
        int[] r = crame(matrix);
        for(int i = 0; i < r.length; i++) {
        	info("x"+(i+1)+" = "+r[i]);
        }
        
        for (int i = 0; i < matrix.length; i++) {
	          int[] base = matrix[i];
	          if (check(base, r, base[3])) {
	             info("System " + (i + 1) + " checks!");
	          } else {
	             info("System " + (i + 1) + " fails check!");
	        }
        }
		

		
		int[][] a = new int[][]{
		        { 8, 2, 8, 7, 7},
		        { 0, 1, 6, 9, 9},
		        { 0, 0, 2, 7, 0},
		        { 0, 0, 0, 3, 9},
		        { 0, 0, 0, 0, 1}
		    };
		    
		
		info("det = "+DeterminantCalculate.execute(a));
	}
	

      
	
	public static int[] crame(int[][] m) {
	    int[] result = new int[] {};
	    result = new int[m.length];
        int D = DeterminantCalculate.execute(m);
        for (int i = 0; i < m.length; i++) {
            result[i] = DeterminantCalculate.execute(slide(m, i, m.length)) / D;
        }
	    return result;
	}
	
	public static int[][] slide(int[][] base, int col, int fin) {
	    int[][] copy = new int[base.length][];
	    for (int i = 0; i < base.length; i++) {
	        int[] aMatrix = base[i];
	        int aLength = aMatrix.length;
	        copy[i] = new int[aLength];
	        System.arraycopy(aMatrix, 0, copy[i], 0, aLength);
	    }
	    for (int i = 0; i < base.length; i++) {
	        copy[i][col] = base[i][fin];
	    }
	    return copy;
	}
	
	public static int product(int[] a, int[] b) {
	    int p = 0;
	    int[] fin = new int[(a.length - 1)];
	    for (int x = 0; x < fin.length; x++) {
	        fin[x] = a[x] * b[x];
	    }
	    for (int f : fin) {
	        p += f;
	    }
	    return p;
	}
	
	public static boolean check(int[] a, int[] b, int z) {
	    return product(a, b) == z;
	}
	
	public static void info(String log) {
	    System.out.println(log);
	}
}
